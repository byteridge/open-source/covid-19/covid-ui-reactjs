import { State } from "../store/reducer";
import {
  getTitle,
  getCovidCountryWise,
  getCovidCountryWiseLoadingState,
} from "../store/selectors";
import { connect } from "react-redux";
import * as React from "react";

import { Table } from "antd";
import { CountryWise } from "../store/Models";
import { ColumnsType } from "antd/lib/table/interface";

interface ConnectProps {
  title: string;
  data: CountryWise[];
  isLoading: boolean;
}

const columns: ColumnsType<CountryWise> = [
  {
    title: "Country",
    dataIndex: "country",
  },
  {
    title: "Confirmed",
    dataIndex: "confirmed",
    sorter: {
      compare: (a: CountryWise, b: CountryWise) => a.confirmed - b.confirmed,
      multiple: 1,
    },
  },
  {
    title: "Active",
    dataIndex: "active",
    sorter: {
      compare: (a: CountryWise, b: CountryWise) => a.active - b.active,
      multiple: 2,
    },
  },
  {
    title: "Recovered",
    dataIndex: "recovered",
    sorter: {
      compare: (a: CountryWise, b: CountryWise) => a.recovered - b.recovered,
      multiple: 3,
    },
  },
  {
    title: "Deaths",
    dataIndex: "deaths",
    sorter: {
      compare: (a: CountryWise, b: CountryWise) => a.deaths - b.deaths,
      multiple: 4,
    },
  },
];

const Covid19tableImpl: React.FunctionComponent<ConnectProps> = (props) => {
  const onChange = (pagination: any, filters: any, sorter: any, extra: any) => {
    console.log("params", pagination, filters, sorter, extra);
  };
  console.log(props);
  return (
    <Table
      columns={columns}
      dataSource={props.data}
      onChange={onChange}
      pagination={false}
      loading={props.isLoading}
    />
  );
};

function mapStateToProps(state: State): ConnectProps {
  return {
    title: getTitle(state),
    data: getCovidCountryWise(state),
    isLoading: getCovidCountryWiseLoadingState(state),
  };
}

const Covid19table = connect(mapStateToProps)(Covid19tableImpl);

export default Covid19table;
