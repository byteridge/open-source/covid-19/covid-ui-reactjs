import { State } from "../store/reducer";
import { getTitle } from "../store/selectors";
import { connect } from "react-redux";
import * as React from "react";

import {
  AzureMap,
  AzureMapsProvider,
  IAzureMapOptions,
  AzureMapLayerProvider,
  AzureMapDataSourceProvider,
} from "react-azure-maps";
import { AuthenticationType } from "azure-maps-control";

interface Props {
  title: string;
}
const option: IAzureMapOptions = {
  authOptions: {
    authType: AuthenticationType.subscriptionKey,
    subscriptionKey: "Da-Z2cCUstIieChbysuNRDKDKTppqr7ug09hZt7sYOo",
  },
  zoom: 0,
};

const bubbleLayerOptions = {
  //Scale the size of the clustered bubble based on the number of points inthe cluster.
  radius: [
    "step",
    ["get", "point_count"],
    20, //Default of 20 pixel radius.
    100,
    30, //If point_count >= 100, radius is 30 pixels.
    750,
    40, //If point_count >= 750, radius is 40 pixels.
  ],

  //Change the color of the cluster based on the value on the point_cluster property of the cluster.
  color: [
    "step",
    ["get", "point_count"],
    "rgba(0,255,0,0.8)", //Default to green.
    100,
    "rgba(255,255,0,0.8)", //If the point_count >= 100, color is yellow.
    750,
    "rgba(255,0,0,0.8)", //If the point_count >= 100, color is red.
  ],
  strokeWidth: 0,
  filter: ["has", "point_count"], //Only rendered data points which have a point_count property, which clusters do.
};

const CountryMapIMpl: React.FunctionComponent<Props> = (props) => {
  return (
    <div style={{ height: "700px" }}>
      <AzureMapsProvider>
        <AzureMap options={option}>
          <AzureMapDataSourceProvider
            id={"BubbleLayer DataSourceProvider"}
            // dataFromUrl="https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_month.geojson"
            options={{
              //Tell the data source to cluster point data.
              cluster: true,

              //The radius in pixels to cluster points together.
              clusterRadius: 45,

              //The maximium zoom level in which clustering occurs.
              //If you zoom in more than this, all points are rendered as symbols.
              clusterMaxZoom: 15,
            }}
          ></AzureMapDataSourceProvider>
        </AzureMap>
      </AzureMapsProvider>
    </div>
  );
};

function mapStateToProps(state: State): Props {
  return {
    title: getTitle(state),
  };
}

const CountryMap = connect(mapStateToProps)(CountryMapIMpl);

export default CountryMap;
