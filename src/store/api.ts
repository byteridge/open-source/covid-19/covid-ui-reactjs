import {
  covidCountryWisePending,
  covidCountryWiseSuccess,
  covidCountryWiseError,
} from "./actions";

export function covidDataCountryWise() {
  return (dispatch: any) => {
    dispatch(covidCountryWisePending());
    fetch("http://40.71.202.138/covid/")
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          throw res.error;
        }
        console.log(res);
        dispatch(covidCountryWiseSuccess(res));
        return res;
      })
      .catch((error) => {
        dispatch(covidCountryWiseError(error));
      });
  };
}
