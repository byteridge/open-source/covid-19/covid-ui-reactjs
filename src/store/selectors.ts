import { State } from "./reducer";
import { CountryWise } from "./Models";

export function getTitle(state: State): string {
  return state.title;
}

export function getCovidCountryWise(state: State): CountryWise[] {
  return state.data;
}

export function getCovidCountryWiseLoadingState(state: State): boolean {
  return state.isLoading;
}
