import { CountryWise } from "./Models";
import {
  FETCH_COVID_COUNTRY_WISE_PENDING,
  FETCH_COVID_COUNTRY_WISE_SUCCESS,
} from "./actions";

export interface State {
  title: string;
  data: CountryWise[];
  isLoading: boolean;
}

const defaultState: State = {
  title: "COVID 19 Tracker",
  data: [],
  isLoading: false,
};

const reducer = function (state: State = defaultState, action: any) {
  switch (action.type) {
    case FETCH_COVID_COUNTRY_WISE_PENDING:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_COVID_COUNTRY_WISE_SUCCESS:
      return {
        ...state,
        data: action.data,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default reducer;
