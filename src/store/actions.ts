import { CountryWise } from "./Models";

export const FETCH_COVID_COUNTRY_WISE_PENDING =
  "FETCH_COVID_COUNTRY_WISE_PENDING";
export const FETCH_COVID_COUNTRY_WISE_SUCCESS =
  "FETCH_COVID_COUNTRY_WISE_SUCCESS";
export const FETCH_COVID_COUNTRY_WISE_ERROR = "FETCH_COVID_COUNTRY_WISE_ERROR";

export function covidCountryWisePending() {
  return {
    type: FETCH_COVID_COUNTRY_WISE_PENDING,
  };
}

export function covidCountryWiseSuccess(data: CountryWise[]) {
  return {
    type: FETCH_COVID_COUNTRY_WISE_SUCCESS,
    data,
  };
}

export function covidCountryWiseError(error: Error) {
  return {
    type: FETCH_COVID_COUNTRY_WISE_ERROR,
    error: error,
  };
}
